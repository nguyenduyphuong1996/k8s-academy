for node in my-k8s-master my-k8s-worker-1 my-k8s-worker-2; do
  node_ip=$(kubectl get node "$node" -o jsonpath='{.status.addresses[?(@.type=="InternalIP")].address}')
  pod_subnet=$(kubectl get node "$node" -o jsonpath='{.spec.podCIDR}')
  gcloud compute routes create "$node" --network=my-k8s-vpc --destination-range="$pod_subnet" \
    --next-hop-address="$node_ip"
done