
for node in my-k8s-master my-k8s-worker-1 my-k8s-worker-2; do tmp=$(mktemp -d)/my-cni-plugin.conf
pod_subnet=$(kubectl get node "$node" -o jsonpath='{.spec.podCIDR}') 
jsonnet -V podSubnet="$pod_subnet" my-cni-plugin.conf.jsonnet >$tmp 
gcloud compute ssh root@"$node" --command "mkdir -p /etc/cni/net.d" 
gcloud compute scp "$tmp" root@"$node":/etc/cni/net.d
done



